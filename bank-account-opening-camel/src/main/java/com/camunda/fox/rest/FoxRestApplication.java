package com.camunda.fox.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * @author Nils Preusker - nils.preusker@camunda.com
 *
 */
@ApplicationPath("rest")
public class FoxRestApplication extends Application {

}
