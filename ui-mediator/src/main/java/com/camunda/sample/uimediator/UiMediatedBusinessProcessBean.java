package com.camunda.sample.uimediator;

import java.util.Map;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Specializes;
import javax.inject.Inject;

import org.activiti.cdi.BusinessProcess;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import com.camunda.sample.tx.Transactional;

/**
 * Specializes {@link BusinessProcess} bean to add generic code needed for UiMediator
 */
@Specializes
public class UiMediatedBusinessProcessBean extends BusinessProcess {
  
  private static final long serialVersionUID = 1L;

  @Inject
  private Event<TaskCompletionEvent> taskCompletionEvent;

  /**
   * we do not have a logged in user in the demo app
   */
  private String currentUser = "kermit";
  
  @Inject
  private UIMediator uiMediator;
  
  @Override
  @Transactional // JPA and Activiti shall do it in the same transaction
  public void completeTask() {
    taskCompletionEvent.fire(new TaskCompletionEvent());

    Task task = getTask();
    super.completeTask();

    if (task != null) {
      uiMediator.checkProcessInstanceStatus(task.getAssignee(), task.getProcessInstanceId());
    }
  }

  @Override
  public ProcessInstance startProcessByKey(String key, Map<String, Object> variables) {
    ProcessInstance processInstance = super.startProcessByKey(key, variables);
    uiMediator.checkProcessInstanceStatus(currentUser, processInstance.getProcessInstanceId());
    return processInstance;
  }
  
}
