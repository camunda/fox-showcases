package com.camunda.sample.customerregistration;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.activiti.cdi.BusinessProcess;
import org.activiti.engine.runtime.ProcessInstance;

import com.camunda.sample.uimediator.TaskCompletionEvent;

@Stateful
@Named
@ConversationScoped
public class CustomerService {

  public static final String VARNAME_CUSTOMER_ID = "customerId";

  public static final String PROCESS_KEY = "ui-mediator";

  @PersistenceContext
  private EntityManager entityManager;

  @Inject
  private BusinessProcess businessProcess;

  private Customer customer;

  @Produces
  @Named("customer")
  public Customer customer() {
    if (customer == null) {
      Long customerId = businessProcess.getVariable(VARNAME_CUSTOMER_ID);
      customer = entityManager.find(Customer.class, customerId);
    }
    return customer;
  }

  public void mergeChanges(@Observes TaskCompletionEvent evt) {
    entityManager.merge(customer);
    customer = null;
  }

  @Produces
  @Named("newCustomer")
  public Customer getNewOrder() {
    if (customer == null) {
      customer = new Customer();
    }
    return customer;
  }

  public ProcessInstance startCustomerRegistration() {
    entityManager.persist(customer);
    entityManager.flush();

    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put(VARNAME_CUSTOMER_ID, customer.getId());
    this.customer = null;

    return businessProcess.startProcessByKey(PROCESS_KEY, variables);
  }

}
