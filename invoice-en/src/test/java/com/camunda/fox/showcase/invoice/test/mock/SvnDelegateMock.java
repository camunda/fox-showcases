package com.camunda.fox.showcase.invoice.test.mock;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;


@Named("svnService")
@ApplicationScoped
public class SvnDelegateMock implements JavaDelegate{

	private boolean called = false;
	
	public void execute(DelegateExecution execution) throws Exception {
		called = true;
	}
	
	public void reset() {
		called = false;
	}
	
	public boolean isCalled() {
		return called;
	}
}
